package Bean;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public abstract class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    public Pet pet;
    private Human mother;
    private Human father;
    long birthDate;

    Map<String, Integer> schedule = new HashMap();


    public Human() {

    }

    public Human(String name, String surname, int year, int iq) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
    }

    public abstract void greetPet();// if it is abstract, no code can be written

    public void describePet() {
        if (pet.getage() > 50)
            System.out.println("I have a " + pet.getSpecies() + ", he is " + pet.getage() + " years old, he is very sly");
        else
            System.out.println("I have a " + pet.getSpecies() + ", he is " + pet.getage() + " years old, he is almost not sly");
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", pet=" + pet +
                ", mother=" + mother +
                ", father=" + father +
                ", birthDate=" + describeAge() +
                ", schedule=" + schedule +
                '}';
    }

    public String describeAge() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        date.setTime((long) birthDate * 1000);
        String text = simpleDateFormat.format(date.getTime());
        return text;
    }
}